from bs4 import BeautifulSoup
import requests
import xlrd
import csv
from datetime import datetime
from urllib.parse import urljoin

# Date format according to ISO 8601
DATE_OUTPUT_FORMAT = {
    'daily': '%Y-%m-%d',
    'weekly': '%Y-W%W',
    'monthly': '%Y-%m',
    'annual': '%Y'
}


def get_html(url):
    response = requests.get(url)
    return BeautifulSoup(response.text, 'html.parser')


def process_xls(workbook, period):
    # Open the 2nd sheet which contains the data
    sheet = workbook.sheet_by_index(1)

    with open(f'data/{period}/data.csv', 'w') as data_file:
        data_file = csv.writer(data_file, delimiter=',')
        data_file.writerow(['Date', 'Price'])

        for row in range(3, sheet.nrows):
            row_data = []

            for col in range(sheet.ncols):
                cell = sheet.cell(row, col)

                if col == 0:
                    # 1st column contains the date
                    # Convert Excel date format to Python date format
                    py_datetime = xlrd.xldate.xldate_as_datetime(
                        cell.value, workbook.datemode)
                    value = py_datetime.strftime(DATE_OUTPUT_FORMAT[period])
                else:
                    # 2nd column contains the price
                    value = cell.value

                row_data.append(value)

            if col == sheet.ncols-1:
                data_file.writerow(row_data)
                row_data.clear()

    print('Write to CSV complete')


def main():
    startUrl = 'https://www.eia.gov/dnav/ng/hist/rngwhhdD.htm'
    base_url = startUrl.rsplit('/', 1)[0] + '/'
    soup = get_html(startUrl)

    # Get url's to the pages for each data period (Daily, Weekly, Monthly, Annual)
    linkElements = soup.find_all('a', {'class': 'NavChunk'})
    links = list(map(lambda el: {
        'period': el.get_text().lower(),
        'url': urljoin(base_url, el['href'])
    }, linkElements))

    for link in links:
        print(f'### {link["period"]} data ###')

        print(f'Opening {link["url"]}')
        soup = get_html(link['url'])
        # Get link to .xls file
        rel_url_xls = soup.select('a[href$="xls"]')[0].attrs['href']
        url_xls = urljoin(base_url, rel_url_xls)

        print('Downloading file:', url_xls)
        response = requests.get(url_xls)
        xls_file_mem = response.content

        workbook = xlrd.open_workbook(file_contents=xls_file_mem)

        print('Processing xls file')
        process_xls(workbook, link['period'])

    print('--> Script finished <--')


if __name__ == "__main__":
    main()
